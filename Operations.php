<?php

use Symfony\Component\Finder\Finder;

class Operations
{
    public function getUrlImag()
    {
        $finder = new Finder();
        $route = __DIR__.'/photos/imagenesG';
        $finder->files()->in($route);
        $images = [];
        foreach ($finder as $file) {
            $ImagSelec = $file->getRelativePathname();
            $images[] = $ImagSelec;
        }
        $length = count($images);
        return $route . '/' . ($images[random_int(1, $length)]);
    }


    public function transporteSMTP()
    {
        $transport = (new Swift_SmtpTransport("smtp.gmail.com", "465", "ssl"))
            ->setUsername('fghuuser@gmail.com')
            ->setPassword('fghuser05060');

        return $transport;
    }

    /**
     *  data the message for send
     *
     * @param string $mail
     * @param string $routeImag
     * @param string $Name
     *
     *
     * @return   Swift_Message
     *
     *
     */
    public function shipping($mail, $routeImag,$name)
    {
        $message = (new Swift_Message("Feliz dia $name desde Praga Web Studio!!"))
            ->setFrom(['fghuuser@gmail.com' => 'UserXtc'])
            ->setTo([$mail])
            ->setBody('Cada dia es un aprendizaje!')
            ->attach(Swift_Attachment::fromPath( $routeImag));
        return $message;
    }
}
