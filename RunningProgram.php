<?php
require "Operations.php";


class RunnigProgram
{

    public function RunApp()
    {
        $llam = new Operations();
        $con = new conectionDB();
        $mailer = new Swift_Mailer($llam->transporteSMTP());

        $consult = "select * from DB_Envios";
        $result = mysqli_query($con->conexion(), $consult) or die ("Exist an error in the Operation Result");

        $routeImag = $llam->getUrlImag();
        while ($column = mysqli_fetch_array($result)) {

            if ($column['active']) {
               $mailer->send($llam->shipping($column['Correo'], $routeImag,$column['nombre_Usuario']));
            }
        }

        $con->close($con->conexion());

    }
}
